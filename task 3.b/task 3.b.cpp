#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void getMarkStr(int mark, char* strMark) {
	char marks[][25] = 
	{
		"",
		"",
		"�����������",
		"���������",
		"�����",
		"������"
	};
	if (mark < 2 || mark > 5) {
		printf("Unknown mark\n");
		exit(-1);
	}
	strcpy(strMark, marks[mark]);
}

int main()
{
	system("chcp 1251"); system("cls");
	int mark; char markStr[15];
	scanf("%d", &mark);
	getMarkStr(mark, markStr);
	printf("%d - %s", mark, markStr);
}
