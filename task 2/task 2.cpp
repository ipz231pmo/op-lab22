#define _CRT_SECURE_NO_WARNINGS
#define _CRT_NONSTDC_NO_DEPRECATE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
char str[100], words[100][100], uniqueWords[100][100];
int wordsCount, uniqueWordsCount, wordCount[100];
void formWords() {
	char* token = strtok(str, " ,.!?");
	while (token)
	{
		strcpy(words[wordsCount++], token);
		token = strtok(NULL, " ,.!?");
	}
}
void countWords() {
	for (int i = 0; i < wordsCount; i++) {

		int findIndex = -1;

		for (int j = 0; j < uniqueWordsCount; j++) {
			if (strcmp(uniqueWords[j], words[i]) == 0) findIndex = j;
		}
		if (findIndex != -1) {
			wordCount[findIndex]++;
		}
		else {
			strcpy(uniqueWords[uniqueWordsCount++], words[i]);
			wordCount[uniqueWordsCount - 1] = 1;
		}

	}
	printf("Words count:\n");
	for (int i = 0; i < uniqueWordsCount; i++) {
		printf("\t%s - %d\n", uniqueWords[i], wordCount[i]);
	}
}
void MaxRepeatedWord() {
	int maxRepWordIndex = 0;
	for (int i = 1; i < uniqueWordsCount; i++) {
		if (wordCount[i] > wordCount[maxRepWordIndex]) maxRepWordIndex = i;
	}
	printf("Max Repeated word is \"%s\" - %d reps\n", uniqueWords[maxRepWordIndex], wordCount[maxRepWordIndex]);
}
void checkPalindroms() {
	for (int i = 0; i < wordsCount; i++) {
		bool pal = true;
		for (int j = 0; j < strlen(words[i]) / 2; i++)
			if (words[i][j] != words[i][strlen(words[i]) - 1 - j]) 
				pal = false;
		if (pal) {
			printf("There is palindroms\n");
			return;
		}
	}
	printf("There isnt palindroms\n");
}
void formUniqueStrAndSort() {
	strcpy(str, "\0");

	for(int i = 0; i < uniqueWordsCount; i++)
		for(int j = i + 1; j < uniqueWordsCount; j++)
			if (strcmp(uniqueWords[i], uniqueWords[j]) > 0) {
				char tmp[100];
				strcpy(tmp, uniqueWords[i]);
				strcpy(uniqueWords[i], uniqueWords[j]);
				strcpy(uniqueWords[j], tmp);
			}

	for (int i = 0; i < uniqueWordsCount; i++) {
		strcat(str, uniqueWords[i]);
		strcat(str, " ");
	}
	printf("Sorted string without repeats: %s\n", str);
}
int main()
{
	gets_s(str);
	formWords();
	checkPalindroms();
	countWords();
	MaxRepeatedWord();
	formUniqueStrAndSort();
}