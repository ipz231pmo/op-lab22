﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

void process(char* str, int* answer) {
    char* delim = strtok(str, " ");
    while (delim){
        int count = 0;
        for (int i = 0; i < strlen(delim); i++) {
            switch (delim[i]){
            case 'a':
            case 'A':
            case 'i':
            case 'I':
            case 'e':
            case 'E':
            case 'y':
            case 'Y':
            case 'u':
            case 'U':
            case 'o':
            case 'O':
                count++;
                break;
            default:
                break;
            }
        }
        if (count > 2) (*answer)++;
        delim = strtok(NULL, " ");
    }
}

int main()
{
    char str[100]; int count = 0;
    gets_s(str);
    process(str, &count);
    printf("%d\n", count);
}