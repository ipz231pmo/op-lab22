#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void getMark(const char* strMark, int* mark) {
	char marks[][25] =
	{
		"�����������",
		"���������",
		"�����",
		"������"
	};
	for (int i = 0; i < 4; i++)
		if (!strcmp(strMark, marks[i])) {
			*mark = i + 2;
			return;
		}
	printf("Unknown mark\n");
	exit(-1);
}

int main()
{
	system("chcp 1251"); system("cls");
	int mark; char markStr[15];
	scanf("%s", &markStr);
	getMark(markStr, &mark);
	printf("%d - %s", mark, markStr);
}
